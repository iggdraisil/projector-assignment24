from fastapi.testclient import TestClient
from pytest import fixture

from project.main import app


@fixture
def client():
    return TestClient(app)


def test_root(client: TestClient):
    resp = client.get(app.url_path_for('root'))
    assert resp.is_success
    assert resp.json() == {"message": f"Hello World"}


def test_hello(client: TestClient):
    resp = client.get(app.url_path_for('hello', name='Mike'))
    assert resp.is_success
    assert resp.json() == {"message": f"Hello Mike"}
