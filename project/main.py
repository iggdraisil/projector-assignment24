from fastapi import FastAPI

app = FastAPI()


@app.get("/", name='root')
async def root():
    return {"message": "Hello World"}


@app.get("/hello/{name}", name='hello')
async def say_hello(name: str):
    return {"message": f"Hello {name}"}
